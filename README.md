# HSA-18 Homework project for database replication

This an HSA-18 Homework project to show database replication.

## Preparation

```
./build.sh
```

#### Read changes from slave 1 to ensure the data is replicated

```
docker exec mysql-s1 sh -c "export MYSQL_PWD=111; mysql -u root mydb -e 'select * from code \G'"
```

#### Read changes from slave 2 to ensure the data is replicated

```
docker exec mysql-s2 sh -c "export MYSQL_PWD=111; mysql -u root mydb -e 'select * from code \G'"
```

#### Result

The data is replicated

#### Turn off mysql-s1 
```
docker stop mysql-s1
```

#### Turn on mysql-s1 
```
docker start mysql-s1
```

#### Result
The data is replicated when a slave 1 is online again

#### Remove a column in database on slave node 
```
docker exec mysql-s1 sh -c "export MYSQL_PWD=111; mysql -u root mydb -e 'ALTER TABLE code DROP COLUMN second_code \G'"
```

#### Result
The column is dropped and the data is still replicated. Slave 1:

```
*************************** 156. row ***************************
first_code: 928
*************************** 157. row ***************************
first_code: 989
```
Slave 2:

```
*************************** 156. row ***************************
 first_code: 928
second_code: 437
*************************** 157. row ***************************
 first_code: 989
second_code: 113
```
