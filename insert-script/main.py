import mysql.connector
import random
from time import sleep

def main(config):
    cnx = mysql.connector.connect(**config)
    cur = cnx.cursor()

    first_code = random.randint(100, 1000)
    second_code = random.randint(100, 1000)
    values = (first_code, second_code)
    
    stmt_insert = "INSERT INTO code (first_code, second_code) VALUES (%s, %s)"
    cur.execute(stmt_insert, values)
    cnx.commit()

    cur.close()
    cnx.close()


if __name__ == '__main__':

    config = {
        'host': 'mysql-m',
        'port': 3306,
        'database': 'mydb',
        'user': 'mydb_user',
        'password': 'mydb_pwd',
        'charset': 'utf8',
        'use_unicode': True,
        'get_warnings': True,
    }

    while True:
        print('Inserting...')
        try:
            main(config)
        except Exception as ex:
            print(f'Error: {str(ex)}')
        sleep(1)
